#include <string>

namespace AbstractFactory
{
class IUnit
{
public:
	virtual std::string GetDesc() = 0;
};

class ElfUnit : public IUnit
{
public:
	std::string GetDesc()
	{
		return "Elf archer";
	}
};

class GnomeUnit : public IUnit
{
public:
	std::string GetDesc()
	{
		return "Gnome warrior";
	}
};
class IBarrackFactory
{
public:
	virtual IUnit* Create() = 0;
};

class ElfBarrackFactory : public IBarrackFactory
{
public:
	IUnit* Create()
	{
		return new ElfUnit;
	}
};

class GnomeBarrackFactory : public IBarrackFactory
{
public:
	IUnit* Create()
	{
		return new GnomeUnit;
	}
};

void CreateUnit(IBarrackFactory* UnitFactory)
{
	IUnit* unit = UnitFactory->Create();
}
}	 // namespace AbstractFactory

namespace Builder
{
class IHousePlan
{
public:
	virtual void setBasement(std::string basement) = 0;
	virtual void setStructure(std::string structure) = 0;
	virtual void setRoof(std::string roof) = 0;
	virtual void setInterior(std::string interior) = 0;
};

class House : public IHousePlan
{
private:
	std::string basement;
	std::string structure;
	std::string roof;
	std::string interior;

public:
	virtual void setBasement(std::string basement) override
	{
		this->basement = basement;
	}
	virtual void setStructure(std::string structure) override
	{
		this->structure = structure;
	}
	virtual void setRoof(std::string roof) override
	{
		this->roof = roof;
	}
	virtual void setInterior(std::string interior) override
	{
		this->interior = interior;
	}
};

class IHouseBuilder
{
public:
	virtual void buildBasement() = 0;
	virtual void buildStructure() = 0;
	virtual void buildRoof() = 0;
	virtual void buildInterior() = 0;
	virtual House* getHouse() = 0;
};

class IglooHouseBuilder : public IHouseBuilder
{
private:
	House* house;

public:
	IglooHouseBuilder()
	{
		this->house = new House();
	}
	virtual void buildBasement() override
	{
		house->setBasement("Ice Bars");
	}
	virtual void buildStructure() override
	{
		house->setStructure("Ice Blocks");
	}
	virtual void buildInterior() override
	{
		house->setInterior("Ice Carvings");
	}
	virtual void buildRoof() override
	{
		house->setRoof("Ice Dome");
	}
	virtual House* getHouse() override
	{
		return this->house;
	}
};

class TipiHouseBuilder : public IHouseBuilder
{
private:
	House* house;

public:
	TipiHouseBuilder()
	{
		this->house = new House();
	}
	virtual void buildBasement() override
	{
		house->setBasement("Wooden Poles");
	}
	virtual void buildStructure() override
	{
		house->setStructure("Wood and Ice");
	}
	virtual void buildInterior() override
	{
		house->setInterior("Fire Wood");
	}
	virtual void buildRoof() override
	{
		house->setRoof("Wood, caribou and seal skins");
	}
	virtual House* getHouse() override
	{
		return this->house;
	}
};

class CivilEngineer
{
private:
	IHouseBuilder* houseBuilder;

public:
	CivilEngineer(IHouseBuilder* houseBuilder)
	{
		this->houseBuilder = houseBuilder;
	}

public:
	House* getHouse()
	{
		return houseBuilder->getHouse();
	}
	void constructHouse()
	{
		houseBuilder->buildBasement();
		houseBuilder->buildStructure();
		houseBuilder->buildRoof();
		houseBuilder->buildInterior();
	}
};
}	 // namespace Builder

namespace Prototype
{
class Monster
{
public:
	virtual ~Monster()
	{
	}
	virtual Monster* Clone() = 0;
};

class Ghost : public Monster
{
public:
	Ghost(int health, int speed) : health_(health), speed_(speed)
	{
	}
	virtual Monster* Clone()
	{
		return new Ghost(health_, speed_);
	}

private:
	int health_;
	int speed_;
};

class Spawner
{
public:
	Spawner(Monster* prototype) : prototype_(prototype)
	{
	}
	Monster* spawnMonster()
	{
		return prototype_->Clone();
	}

private:
	Monster* prototype_;
};
}	 // namespace Prototype

namespace Singleton
{

class PS3FileSystem;
class WiiFileSystem;

class FileSystem
{
public:
	static FileSystem& Instance()
	{
#if PLATFORM == PLAYSTATION3
		static FileSystem* instance = new PS3FileSystem();
#elif PLATFORM == WII
		static FileSystem* instance = new WiiFileSystem();
#endif
		return *instance;
	}
	virtual ~FileSystem()
	{
	}
	virtual char* ReadFile(char* path) = 0;
	virtual void WriteFile(char* path, char* contents) = 0;

protected:
	FileSystem()
	{
	}
};

class PS3FileSystem : public FileSystem
{
public:
	virtual char* ReadFile(char* path) override
	{
	}
	virtual void WriteFile(char* path, char* contents) override
	{
	}
};

class WiiFileSystem : public FileSystem
{
public:
	virtual char* ReadFile(char* path) override
	{
	}
	virtual void WriteFile(char* path, char* contents) override
	{
	}
};
}	 // namespace Singleton

int main()
{
	using namespace AbstractFactory;
	IBarrackFactory* Factory = new ElfBarrackFactory;
	CreateUnit(Factory);
	Factory = new GnomeBarrackFactory;
	CreateUnit(Factory);	//������ ������ � ��������

	using namespace Builder;
	IHouseBuilder* iglooBuilder = new IglooHouseBuilder();
	CivilEngineer* engineer = new CivilEngineer(iglooBuilder);
	engineer->constructHouse();
	House* house = engineer->getHouse();	//������ �����

	using namespace Prototype;
	Monster* ghostPrototype = new Ghost(15, 3);
	Spawner* ghostSpawner = new Spawner(ghostPrototype);	//������ ��������� ������ �� ������ ���������

	using namespace Singleton;
	bool ReadFile = FileSystem::Instance().ReadFile(new char());	//������ ���� �� ���������� ���� ���� ������������������ �������
}